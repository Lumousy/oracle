# 实验2：用户及权限管理

杨常玉 202010414123

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale1，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale1连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale1，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
SQL> CREATE ROLE con_res_role;
角色已创建。
SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;
授权成功。
SQL> CREATE USER sale1 IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
用户已创建。
SQL> ALTER USER sale1 default TABLESPACE "USERS";
用户已更改。
SQL> ALTER USER sale1 QUOTA 50M ON users;
用户已更改。
SQL> GRANT con_res_role TO sale1;
授权成功。
```

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale1用户访问users表空间，空间限额是50M。

- 第2步：新用户sale1连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
[oracle@oracle1 ~]$  sqlplus sale1/123@pdborcl
SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 09:26:47 2023
Copyright (c) 1982, 2016, Oracle.  All rights reserved
连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL>  show user;
USER 为 "SALE1"
SELECT * FROM session_privs;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。

SQL> SELECT * FROM session_roles;

ROLE
--------------------------------------------------------------------------------
CON_RES_ROLE
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'ycy');
INSERT INTO customers(id,name)VALUES (2,'xy');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
表已创建。
SQL> 
已创建 1 行。
SQL> 
已创建 1 行。
SQL> 
视图已创建。
SQL> 
授权成功。
SQL> SELECT * FROM customers_view;
NAME
--------------------------------------------------
ycy
xy


SQL> GRANT UPDATE ON customers_view TO hr;
授权成功。

SQL> REVOKE UPDATE ON customers_view FROM hr;
撤销成功。

SQL> REVOKE SELECT ON customers_view FROM hr;
撤销成功。
```

- 第3步：用户hr连接到pdborcl，查询sale1授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL>  SELECT * FROM sale1.customers_view;
NAME
--------------------------------------------------
ycy
xy

SQL> UPDATE sale1.customers_view SET NAME = 'lalala';
已更新 2 行。

SQL> SELECT * FROM sale1.customers_view;
NAME
--------------------------------------------------
lalala
lalala

SQL> SELECT * FROM sale1.customers_view;
SELECT * FROM sale1.customers_view
                    *
第 1 行出现错误:
ORA-00942: 表或视图不存在
```

> 测试一下用户hr,sale1之间的表的共享，只读共享和读写共享都测试一下。
> sale1用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale1  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale1。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SQL> SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;  2    3    4    5    6    7    8  

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      370      26.75	 343.25      92.77
UNDOTBS1			      100	  36	     64 	64
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260     9.3125   250.6875      96.42
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl

SQL> drop role con_res_role;
drop user sale1 cascade;
角色已删除。
```
