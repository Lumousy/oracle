# 实验1：SQL语句的执行计划分析与优化指导

软件工程一班	杨常玉	202010414123

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba

SQL> @$ORACLE_HOME/sqlplus/admin/plustrce.sql
SQL> 
SQL> drop role plustrace;

角色已删除。

SQL> create role plustrace;

角色已创建。

SQL> 
SQL> grant select on v_$sesstat to plustrace;

授权成功。

SQL> grant select on v_$statname to plustrace;

授权成功。

SQL> grant select on v_$mystat to plustrace;

授权成功。

SQL> grant plustrace to dba with admin option;

授权成功。

SQL> 
SQL> set echo off
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;

授权成功。

GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;

授权成功。

SQL> 
授权成功。

SQL> 
授权成功。

SQL> 
授权成功。

SQL> GRANT SELECT ON v_$parameter TO hr; 

```

- 教材中的查询语句：查询两个部门('IT'和'Sales')的部门总人数和平均工资，两个查询的结果是一样的。但效率不相同。

查询1：

```SQL

SQL> set autotrace on

SQL> SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;
  2    3    4    5    6  
DEPARTMENT_NAME                部门总人数   平均工资
------------------------------ ---------- ----------
IT					5	5760
Sales				       34 8955.88235


执行计划
----------------------------------------------------------
Plan hash value: 3808327043

--------------------------------------------------------------------------------

| Id  | Operation		      | Name		  | Rows  | Bytes | Cost
 (%CPU)| Time	  |

--------------------------------------------------------------------------------

|   0 | SELECT STATEMENT	      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   2 |   NESTED LOOPS		      | 		  |    19 |   437 |
4   (0)| 00:00:01 |

|   3 |    NESTED LOOPS 	      | 		  |    20 |   437 |
4   (0)| 00:00:01 |

|*  4 |     TABLE ACCESS FULL	      | DEPARTMENTS	  |	2 |    32 |
3   (0)| 00:00:01 |

|*  5 |     INDEX RANGE SCAN	      | EMP_DEPARTMENT_IX |    10 |	  |
0   (0)| 00:00:01 |

|   6 |    TABLE ACCESS BY INDEX ROWID| EMPLOYEES	  |    10 |    70 |
1   (0)| 00:00:01 |

--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	210  recursive calls
	  0  db block gets
	305  consistent gets
	  0  physical reads
	  0  redo size
	797  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 15  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

```

- 查询2

```SQL

SQL> set autotrace on

SQL> SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');
  2    3    4    5    6  
DEPARTMENT_NAME                部门总人数   平均工资
------------------------------ ---------- ----------
IT					5	5760
Sales				       34 8955.88235


执行计划
----------------------------------------------------------
Plan hash value: 2128232041

--------------------------------------------------------------------------------
--------------

| Id  | Operation		       | Name	     | Rows  | Bytes | Cost (%CP
U)| Time     |

--------------------------------------------------------------------------------

|   0 | SELECT STATEMENT	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|*  1 |  FILTER 		       |	     |	     |	     |
  |	     |

|   2 |   HASH GROUP BY 	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|   3 |    MERGE JOIN		       |	     |	 106 |	2438 |	   6  (1
7)| 00:00:01 |

|   4 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	  27 |	 432 |	   2   (
0)| 00:00:01 |

|   5 |      INDEX FULL SCAN	       | DEPT_ID_PK  |	  27 |	     |	   1   (
0)| 00:00:01 |

|*  6 |     SORT JOIN		       |	     |	 107 |	 749 |	   4  (2
5)| 00:00:01 |

|   7 |      TABLE ACCESS FULL	       | EMPLOYEES   |	 107 |	 749 |	   3   (
0)| 00:00:01 |

--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")


统计信息
----------------------------------------------------------
	184  recursive calls
	  0  db block gets
	281  consistent gets
	  0  physical reads
	  0  redo size
	797  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 14  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

```

* 性能分析

  ```text
  第一种方法：
  	210 递归调用
  	  0 db 块获取
  	305 一致获取
  	  0 物理读取
  	  0 重做大小
  	797 字节通过 SQL*Net 发送到客户端
  	通过 SQL*Net 从客户端接收 608 字节
  	  2 次往返客户端的 SQL*网络往返
  	 15 种（内存）
  	  0 排序（磁盘）
  	  处理 2 行
  
  第二种方法：
  	184 次递归调用
  	  0 db 块获取
  	281 一致获取
  	  0 物理读取
  	  0 重做大小
  	797 字节通过 SQL*Net 发送到客户端
  	通过 SQL*Net 从客户端接收 608 字节
  	  2 次往返客户端的 SQL*网络往返
  	 14 种（内存）
  	  0 排序（磁盘）
  	  处理 2 行
  ```

* 可以看出第二种方法递归调用的次数更少，
