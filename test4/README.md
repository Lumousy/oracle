# 实验4：PL/SQL语言打印杨辉三角

杨常玉/202010414123

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 创建杨辉三角源代码

```sql
CREATE OR REPLACE PROCEDURE YHTRIANGLE (N IN INTEGER) AS
  TYPE T_NUMBER IS VARRAY(100) OF INTEGER NOT NULL;
  ROW_ARRAY T_NUMBER := T_NUMBER();
  I INTEGER;
  J INTEGER;
  SPACES VARCHAR2(30) := '   ';
BEGIN
  DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行

  
  -- 初始化数组数据
  FOR I IN 1..N LOOP
    ROW_ARRAY.EXTEND;
  END LOOP;
  ROW_ARRAY(1) := 1;
  ROW_ARRAY(2) := 1;    
  
  -- 打印每行，从第2行起
  FOR I IN 2..N LOOP
    ROW_ARRAY(I) := 1;    
    J := I - 1;
    
    -- 准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
    -- 这里从第j-1个数字循环到第2个数字，顺序是从右到左
    WHILE J > 1 LOOP
      ROW_ARRAY(J) := ROW_ARRAY(J) + ROW_ARRAY(J-1);
      J := J - 1;
    END LOOP;
    
    -- 打印第i行
    FOR J IN 1..I LOOP
      DBMS_OUTPUT.PUT(RPAD(ROW_ARRAY(J), 9, ' '));
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE(''); --打印换行
  END LOOP;
END;
```

# 调用存储过程

```sql
SET SERVEROUTPUT ON;

began
YHTRIANGLE(10);
end;
```
